# week1

## Lesson November 14, 2023 9am-11am

1. Module 2 introduction: [pdf](./slides/Machine_Learning_Systems_For_Data_Science.pdf)
2. Jupyter Notebook: [pdf](./slides/Machine_Learning_Systems_For_Data_Science_Notebook.pdf)
 - Markdown cell: [notebook](./scripts/Markdown_cell.ipynb)
3. Python Libraries: [pdf](./slides/Machine_Learning_Systems_For_Data_Science_Libraries.pdf)

## Lesson November 14, 2023 4pm-6pm
1. Markdown cell: [notebook](./scripts/Markdown_cell.ipynb)
 - Markdown exercises: [notebook](./exercises/Markdown_exercises.ipynb)
 - Markdown exercises solution: [notebook](./exercises/Markdown_exercises_solution.ipynb)
2. Check python libraries' versions: [notebook](./scripts/check_python_modules.ipynb)
3. Setup script: [notebook](./scripts/setup.ipynb)
4. Numpy Library: [notebook](./scripts/numpy_library.ipynb)
 - Numpy exercises: [notebook](./exercises/Numpy_exercises.ipynb)
5. Pandas Library
 - Series pandas: [notebook](./scripts/series_pandas.ipynb)
 - Series exercises: [notebook](./exercises/Series_pandas_exercises.ipynb)
 - Series exercises solution: [notebook](./exercises/Series_pandas_exercises_solution.ipynb)

## Lesson November 15, 2023 2pm-4pm
1. Pandas Library
 - Dataframe pandas: [notebook](./scripts/dataframe_pandas.ipynb)
 - Dataframe exercises: [notebook](./exercises/Dataframe_pandas_exercises.ipynb)
2. Visualization Library
 - Matplotlib: [notebook](./scripts/matplotlib_library.ipynb)

## Lesson November 16, 2023 2pm-4pm
1. Visualization Library
 - Matplotlib: [notebook](./scripts/matplotlib_library.ipynb)
 - Matplotlib exercises: [notebook](./exercises/Matplotlib_exercises.ipynb)
 - Matplotlib exercises solution: [notebook](./exercises/Matplotlib_exercises_solution.ipynb)
 - Seaborn library: [notebook](./scripts/seaborn_library.ipynb)
 - Seaborn exercises: [notebook](./exercises/Seaborn_exercises.ipynb)
 - Seaborn exercises solution: [notebook](./exercises/Seaborn_exercises_solution.ipynb)
 - Plotly library: [notebook](./scripts/plotly_library.ipynb)
2. Use cases
 - Hanoi Air quality data exploration: [notebook](./use_cases/air_quality_data_visalization.ipynb)
 - California housing price data exploration: [notebook](./use_cases/california_housing_price_data_exploration.ipynb)
 - Italian Covid data exploration: [notebook](./use_cases/covid_data.ipynb)
 - Top US cities data exploration: [notebook](./use_cases/top_us_cities_data.ipynb)
3. Homework
 - Review material week1
 - Do homework week1: [project](https://gitlab.com/90477_mls_4ds_iii/homework_week1)
 - Homework week1 solution: [project](https://gitlab.com/90477_mls_4ds_iii/homework_week1_solution) 
 

